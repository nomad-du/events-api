const schedule = require('node-schedule');
const request  = require('request-promise');
const moment   = require('moment');

const scheduleUpdate = (update, apiUrl) => (timestamp, userId, event) => {
  if (!timestamp) return;
  const date = moment(timestamp);

  // we schedule update to now if startDate is passed
  const now = moment();
  const scheduleDate = date.isBefore(now) ? now.add(10, 's') : date;

  const url = `${apiUrl}/states/${userId}/update`;
  const body = update === EVENT_START
    ? { currentEvent: event }
    : { currentEvent : null };

  console.log('schedule update for ', userId, event.name, 'at', scheduleDate);

  schedule.scheduleJob(scheduleDate.toDate(), () => {
    request
      .post(url, { json: true, body })
      .then(() => console.log('post update for ', userId, event))
      .catch((err) => console.error(err));
  })
}

const EVENT_START = 'EVENT_START';
const EVENT_END   = 'EVENT_END';

module.exports = ({ apiUrl }) => ({
  startEvent: scheduleUpdate(EVENT_START, apiUrl),
  endEvent:   scheduleUpdate(EVENT_END, apiUrl),
});
