const { Client } = require('pg');

const createEventsClient = async (config) => {
  const client = new Client(config);
  try {
    await client.connect();
  } catch(e) {
    console.error(e);
    process.exit(-1);
  }

  return {
    getAll: async () => {
      const query = `SELECT * from events`;

      const result = await client.query(query);
      return result.rows ? result.rows : [];
    },
    create: async (event) => {
      const query = `
        INSERT INTO events(id, name, description, user_id, start_date, end_date,
        type, source, raw_data) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9)
        ON CONFLICT DO NOTHING
      `;

      const values = [
        event.id,
        event.name,
        event.description,
        event.userId,
        event.startDate,
        event.endDate,
        'agenda',
        event.source,
        event.raw,
      ];

      return client.query(query, values);
    },
  };
};

module.exports = createEventsClient;
