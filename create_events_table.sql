CREATE TABLE events (
  id          TEXT PRIMARY KEY,
  name        TEXT,
  description TEXT,

  user_id     TEXT,

  start_date  TIMESTAMP,
  end_date    TIMESTAMP,
  type        TEXT,
  source      TEXT,

  raw_data    JSONB,

  created_at  TIMESTAMP DEFAULT now()
)

