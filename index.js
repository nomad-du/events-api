const app = require('express')();
const bodyParser = require('body-parser');
const dotenv = require('dotenv');
const moment = require('moment');

dotenv.config();

const PGUSER = (() => {
  if (!process.env.PGUSER) {
    throw new Error('Need PGUSER env var');
  }

  return process.env.PGUSER;
})();

const PGPASSWORD = (() => {
  if (!process.env.PGPASSWORD) {
    throw new Error('Need PGPASSWORD env var');
  }

  return process.env.PGPASSWORD;
})();

const PGHOST = (() => {
  if (!process.env.PGHOST) {
    throw new Error('Need PGHOST env var');
  }

  return process.env.PGHOST;
})();

const PGPORT = (() => {
  if (!process.env.PGPORT) {
    throw new Error('Need PGPORT env var');
  }

  return process.env.PGPORT;
})();

const PGDATABASE = (() => {
  if (!process.env.PGDATABASE) {
    throw new Error('Need PGDATABASE env var');
  }

  return process.env.PGDATABASE;
})();

const STATE_API_URL = (() => {
  if (!process.env.STATE_API_URL) {
    throw new Error('Need STATE_API_URL env var');
  }

  return process.env.STATE_API_URL;
})();

const PORT = process.env.PORT || 5000;

const createEventsClient = require('./src/events');
const scheduler = require('./src/scheduler')({ apiUrl: STATE_API_URL });

app.use(bodyParser.json());

const run = async () => {
  const events = await createEventsClient({
    host:     PGHOST,
    port:     PGPORT,
    user:     PGUSER,
    password: PGPASSWORD,
    database: PGDATABASE,
  });

  app.get('/events', async (req, res) => {
    try {
      const result = await events.getAll();

      res.status(200).json({ events: result });
    } catch(e) {
      console.error(e);
      res.sendStatus(500);
    }
  });

  app.post('/events', async ({ body }, res) => {
    try {
      const event = body.event;
      event.raw = { ...body.event };

      // if no start or end date event is useless
      if (!(event.startDate || event.endDate)) {
        res.status(400).json({ error: 'Missing start or end date' });
        return;
      }

      console.log('create event', event);

      await events.create(event);

      scheduler.startEvent(event.startDate, event.userId, event)
      scheduler.endEvent(event.endDate, event.userId, event)

      res.sendStatus(204);
    } catch(e) {
      console.error(e);
      res.sendStatus(500);
    }
  });
};

run();

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));

